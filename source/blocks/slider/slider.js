import Swiper from 'swiper/dist/js/swiper';

export default class Slider {
  constructor(parent) {
    this.slider = parent.querySelector('.slider__container');

    if (this.slider) this.initSlider();
  }

  initSlider() {
    new Swiper(this.slider, {
      slidesPerView: 1,
      loop: true,
      autoplay: 5000,
      navigation: {
        prevEl: '.slider__arrow-prev',
        nextEl: '.slider__arrow-next',
      },
    });
  }
}
