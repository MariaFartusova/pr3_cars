export default class Menu {
  constructor(parent) {
    this.menu = parent;
    this.menuBtn = parent.querySelector('.menu__burger');
    this.menuContent = parent.querySelector('.menu__content');

    if (this.menu && this.menuBtn) this.initEvents();
  }

  initEvents() {
    this.menuBtn.addEventListener('click', (event) => {
      event.preventDefault();
      this.menu.classList.toggle('menu--open');
      this.menuBtn.classList.toggle('menu__burger--open');
      this.menuContent.classList.toggle('menu__content--open');
    });
  }
}
