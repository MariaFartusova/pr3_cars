import 'svgxuse';
import 'custom-event-polyfill';
import ObjectFitImages from 'object-fit-images';
import Slider from './blocks/slider/slider';
import Menu from './blocks/menu/menu';

require('./autoload.scss');

ObjectFitImages(null, { watchMQ: true });

const menu = document.querySelector('.menu');

if (menu) {
  new Menu(menu);
}

const sliders = [].slice.call(document.querySelectorAll('.slider'));

sliders.forEach((item) => {
  if (item) {
    new Slider(item);
  }
});


// [].slice.call(document.querySelectorAll('.slider').forEach((item) => {
//   if (item) {
//     new Slider(item);
//   }
// }));
